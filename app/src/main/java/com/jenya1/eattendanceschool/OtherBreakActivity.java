package com.jenya1.eattendanceschool;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.jenya1.eattendanceschool.uploadutil.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OtherBreakActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private EditText inputEmail;
    TextView title,conformation;
    ImageView showImageView,emoji;
    LinearLayout lnoperation,in,out,lnemoji;

    private ProgressBar progressBar;

    String email,date;
    int maxLength,logcheck=0,min;

    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude=" ",longitude=" ";

    boolean previewing = false;

    String name,imageFilePath;
    int count=0;


    Bitmap image;

    String URL_LOGIN = "http://www.rayvatapps.com:8001//api.php?request=login";

    String URL_OTHER_BREAK_START ="http://www.rayvatapps.com:8001//api.php?request=startbreak";

    String URL_OTHER_BREAK_END ="http://www.rayvatapps.com:8001//api.php?request=endbreak";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_break);

        inputEmail = (EditText) findViewById(R.id.email);
        conformation=(TextView)findViewById(R.id.tv_conformation);
        showImageView=(ImageView) findViewById(R.id.img_preview);
        emoji=(ImageView) findViewById(R.id.imgemoji);
        lnoperation=(LinearLayout) findViewById(R.id.lnoperation);
        in=(LinearLayout) findViewById(R.id.lnin);
        out=(LinearLayout) findViewById(R.id.lnout);
        lnemoji=(LinearLayout) findViewById(R.id.lnemoji);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        surfaceView = (SurfaceView)findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        Glide.with(OtherBreakActivity.this)
                .load(R.drawable.otherbreak)
                .into(emoji);

        maxLength = 4;
        inputEmail.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(inputEmail.getText().toString().length()>= maxLength) {
                    //Show toast here
                    email = inputEmail.getText().toString().trim();

                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(inputEmail.getWindowToken(), 0);
                    // password = inputPassword.getText().toString().trim();

                    if (TextUtils.isEmpty(email)) {
                        inputEmail.setError("Enter email address!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                        return false;
                    }

               /* if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                    progressBar.setVisibility(View.VISIBLE);

                    checklogin();

//                            login.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                                    login.setEnabled(true);
                        }
                    }, 3000);
                }
                return false;
            }
        });

        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=1;

                checkwork(URL_OTHER_BREAK_START);

                in.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in.setEnabled(true);
                    }
                }, 3000);
            }
        });
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=2;
//                camera.takePicture(null, null, null, jpegCallback);

                checkwork(URL_OTHER_BREAK_END);

                out.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        out.setEnabled(true);
                    }
                }, 3000);
            }
        });

    }

    protected void showCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

    }

    @SuppressLint("LongLogTag")
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(OtherBreakActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (OtherBreakActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(OtherBreakActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

                /*Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);

            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

                /*Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);



            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

                /*Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);

            }else{

                longitude="76.76";
                lattitude="23.23";

                /*List<String> providers = locationManager.getProviders(true);
                Location bestLocation = null;
                for (String provider : providers) {
                    Location l = locationManager.getLastKnownLocation(provider);
                    double latti = l.getLatitude();
                    double longi = l.getLongitude();
                    lattitude = String.valueOf(latti);
                    longitude = String.valueOf(longi);

                    Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                            "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();

                    Log.e("last known location, provider: %s, location: %s", provider+ l);

                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null
                            || l.getAccuracy() < bestLocation.getAccuracy()) {
                        Log.e("found best last known location: %s", String.valueOf(l.getLatitude()));
                        bestLocation = l;
                        return;
                    }
                }
                if (bestLocation == null) {
                    Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();
//                    return null;
                }*/

                Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();

            }
        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 3000;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        TextView txt = (TextView) findViewById(R.id.usage);
                        if(conformation.getVisibility() == View.VISIBLE){
                            conformation.setVisibility(View.INVISIBLE);
                        }else{
                            conformation.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }

    private void checkauthitication() {
        lnemoji.setVisibility(View.VISIBLE);
        //emoji.setBackgroundResource(R.drawable.lunch_start);

       /* File fdelete = new File(imageFilePath);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + imageFilePath);
            } else {
                System.out.println("file not Deleted :" + imageFilePath);
            }
        }*/

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Intent i=new Intent(OtherBreakActivity.this,OtherBreakActivity.class);
                startActivity(i);
                finish();

                //Toast.makeText(TrackerActivity.this, "toast", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    private void checklogin() {
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            //Log.e("request",ServerResponse);
                            JSONObject jObj = new JSONObject(ServerResponse);

                            String status=jObj.getString("status");

                            String msg =jObj.getString("message");

                            if (status.equals("1")) {

                                        /*Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();*/

                                //conformation.setVisibility(View.VISIBLE);
                                lnoperation.setVisibility(View.VISIBLE);
                                inputEmail.setVisibility(View.GONE);

                                in.setEnabled(true);
                                out.setEnabled(true);
                                logcheck=1;
                                //Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                //conformation.setVisibility(View.GONE);
                                lnoperation.setVisibility(View.GONE);
                                showImageView.setVisibility(View.GONE);
                                inputEmail.setVisibility(View.VISIBLE);
                                in.setEnabled(false);
                                out.setEnabled(false);

                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);

                            getUsername();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                date=sdf.format(new Date());
                // Adding All values to Params.

                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(OtherBreakActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getUsername() {

        String JSON_USERNAME=URL_LOGIN+"&username="+email+"&date="+date;
        Log.e("da",JSON_USERNAME);
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_USERNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("response");

                            String uname=heroArray.getString("Name");

                            String Image=heroArray.getString("Avatar");

                            String job_in_time=heroArray.getString("job_in_time");

                            String lunch_in_time=heroArray.getString("lunch_in_time");

                            String tea_in_time=heroArray.getString("tea_in_time");

                            String job_out_time=heroArray.getString("job_out_time");

                            String lunch_out_time=heroArray.getString("lunch_out_time");

                            String tea_out_time=heroArray.getString("tea_out_time");

                            Toast.makeText(OtherBreakActivity.this, "Welcome "+uname, Toast.LENGTH_SHORT).show();

                            byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);

                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            showImageView.setVisibility(View.VISIBLE);

//                            showImageView.setImageBitmap(decodedByte);

                            Glide.with(OtherBreakActivity.this)
                                    .load(decodedString)
                                    .asBitmap()
                                    .placeholder(R.drawable.user)
                                    .into(showImageView);

                            showCurrentLocation();

                           /* SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                            String time=sdft.format(new Date());
                            Date current = null;
                            try {
                                current = new SimpleDateFormat("HH:mm:ss").parse(time);
                                Date db = new SimpleDateFormat("HH:mm:ss").parse(job_in_time);

                                long different = current.getTime() - db.getTime();

                                Log.e("difference ", String.valueOf(different));
                                if (different < 700000) {
                                    min = 1;
                                    //                                        Toast.makeText(LoginActivity.this, "wait 10 minute to logout", Toast.LENGTH_SHORT).show();
                                } else {
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    inputEmail.setVisibility(View.GONE);
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }*/








                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void checkwork(final String URL) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        Log.e("server",ServerResponse);
                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();
//                                camera.takePicture(null, null, null, jpegCallback);

                                checkauthitication();

                            } else {
                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());

                SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                String time=sdft.format(new Date());


                /*Log.e("email "+email,"\ndate "+date);
                Log.e("type "+status+"\nimage "+ main.getBytes().length,"\npunch_time "+time);*/

                // Adding All values to Params.
                //params.put("name", name);
                params.put("username", email);
                params.put("lat", lattitude);
                params.put("long", longitude);
                params.put("date", date);
                params.put("time", time);

                return params;
            }

        };

        MySingleton.getInstance(OtherBreakActivity.this).addToRequestQueue(stringRequest);
       /* // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);*/
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {

            FileOutputStream outStream = null;
            try {
                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance");

                if (!direct.exists()) {
                    direct.mkdirs();
                }
                name=email+ System.currentTimeMillis()+".jpg";
                imageFilePath = direct+"/"+name;
                Uri selectedImage = Uri.parse(imageFilePath);
                File file = new File(imageFilePath);
                String path = file.getAbsolutePath();
                Bitmap bitmap = null;
                // Directory and name of the photo. We put system time
                // as a postfix, so all photos will have a unique file name.
                outStream = new FileOutputStream(file);
                outStream.write(data);
                outStream.close();

                if (path != null) {
                    if (path.startsWith("content")) {
                        bitmap = decodeStrem(file, selectedImage,
                                OtherBreakActivity.this);
                    } else {
                        bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                    }
                    Log.e("bitmap", String.valueOf(bitmap));
                }
                if (bitmap != null) {
                    image=bitmap;
                    Toast.makeText(OtherBreakActivity.this,
                            "Picture Captured Successfully", Toast.LENGTH_LONG)
                            .show();

                    if(count==1)
                    {
                        checkwork(URL_OTHER_BREAK_START);
                    }
                    else if (count==2)
                    {
                        checkwork(URL_OTHER_BREAK_END);
                    }

                } else {
                    Toast.makeText(OtherBreakActivity.this,
                            "Failed to Capture the picture. kindly Try Again:",
                            Toast.LENGTH_LONG).show();
                }




            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }

        }
    };

    public Bitmap decodeStrem(File fil, Uri selectedImage,
                              Context mContext) {

        Bitmap bitmap = null;
        try {

            bitmap = BitmapFactory.decodeStream(mContext.getContentResolver()
                    .openInputStream(selectedImage));
            /*Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
            img = ConvertBitmapToString(resizedBitmap);*/
            final int THUMBNAIL_SIZE = getThumbSize(bitmap);

            bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE,
                    THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baos
                    .toByteArray()));

            return bitmap = rotateImage(bitmap, fil.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap decodeFile(File f, int sampling) {
        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);

            o2.inSampleSize = sampling;
            o2.inTempStorage = new byte[48 * 1024];

            o2.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            bitmap = rotateImage(bitmap, f.getAbsolutePath());
            //img = ConvertBitmapToString(bitmap);

            return bitmap;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getThumbSize(Bitmap bitmap) {

        int THUMBNAIL_SIZE = 250;
        if (bitmap.getWidth() < 300) {
            THUMBNAIL_SIZE = 250;
        } else if (bitmap.getWidth() < 600) {
            THUMBNAIL_SIZE = 500;
        } else if (bitmap.getWidth() < 1000) {
            THUMBNAIL_SIZE = 750;
        } else if (bitmap.getWidth() < 2000) {
            THUMBNAIL_SIZE = 1500;
        } else if (bitmap.getWidth() < 4000) {
            THUMBNAIL_SIZE = 2000;
        } else if (bitmap.getWidth() > 4000) {
            THUMBNAIL_SIZE = 2000;
        }
        return THUMBNAIL_SIZE;
    }

    public static Bitmap rotateImage(Bitmap bmp, String imageUrl) {
        if (bmp != null) {
            ExifInterface ei;
            int orientation = 0;
            try {
                ei = new ExifInterface(imageUrl);
                orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

            } catch (IOException e) {
                e.printStackTrace();
            }
            int bmpWidth = bmp.getWidth();
            int bmpHeight = bmp.getHeight();
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED:
                    matrix.postRotate(270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }
            Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmpWidth,
                    bmpHeight, matrix, true);
            return resizedBitmap;
        } else {
            return bmp;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub
        // stop the camera
        if(previewing){
            camera.stopPreview(); // stop preview using stopPreview() method
            previewing = false; // setting camera condition to false means stop
        }
        // condition to check whether your device have camera or not
        if (camera != null){
            try {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE); //EFFECT_SEPIA //applying effect on camera
                camera.setParameters(parameters); // setting camera parameters
                camera.setPreviewDisplay(surfaceHolder); // setting preview of camera
                camera.startPreview();  // starting camera preview

                previewing = true; // setting camera to true which means having camera
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
		/*int cameraId = 0;
		for(int i=0;i<Camera.getNumberOfCameras();i++){
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId,info);
			if(info.facing== Camera.CameraInfo.CAMERA_FACING_FRONT){
				cameraId = i;
				break;
			}
		}*/
        try
        {
            camera = Camera.open(1);
            //camera = Camera.open();   // opening camera
            camera.setDisplayOrientation(90);   // setting camera preview orientation
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Front camera not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        camera.stopPreview();  // stopping camera preview
        camera.release();       // releasing camera
        camera = null;          // setting camera to null when left
        previewing = false;   // setting camera condition to false also when exit from application
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.with(getApplicationContext()).pauseRequests();
    }

}
